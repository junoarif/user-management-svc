const env = {
    database: 'widya_db',
    username: 'postgres',
    password: 'lupakan123',
    host: '127.0.0.1',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
  };
   
  module.exports = env;
  